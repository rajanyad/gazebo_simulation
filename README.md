After cloning this repo do the following steps to run the simulation:

## Compile and Build

1. Edit ~/.bashrc and enter the following lines at the end: 
	source ~/mybot_ws/devel/setup.bash
	export GAZEBO_MODEL_PATH=$HOME/mybot_ws/src/mybot_gazebo/models:$GAZEBO_MODEL_PATH
2. Open terminal and run: $ source ~/.bashrc
3. $ cd ~/mybot_ws
4. $ catkin_make

---

## Run

1. $ ./run_gazebo.sh
2. $ cd devel/lib/mybot_gazebo
3. $ ./cmd_pos
4. Enter arrow keys to change the position of the models.